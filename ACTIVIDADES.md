## Especificaciones

Crear un carrito de compras (delivery)

## Características

## Historias de usuario

Como usuario UNIDAD_PRODUCTIVA

- Crear un producto (campos nombre, codigo, precio)           (menú de productos)
- Listar todos los productos (nombre, codigo, precio, estado)  (menú de productos)

Como usuario CLIENTE

- Registrar un pedido   (menú de pedidos)
- Listar todos pedidos  (menú de pedidos)














## MODELO DE BASE DE DATOS

# esquema usuarios

usuario
--------

rol
--------

usuario_rol
--------

# esquema productos

producto
--------
- id
- nombre
- codigo
- precio

# esquema pedidos

pedido
------
- id
- nro_pedido
- fecha_pedido

detalle_pedido
--------
- id
- id_producto
- id_pedido
- cantidad


## Módulos

- usuarios   acciones: autenticar, obtenerPerfil, cambiarRol
- productos  acciones: crearProducto, listarProductos
- pedidos    acciones: registrarPedido (buscarProductoPorCodigo), listarPedidos

