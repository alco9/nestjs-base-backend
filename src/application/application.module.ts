import { Module } from '@nestjs/common'
import { ParametricasModule } from './parametricas/parametricas.module'
import { PedidosModule } from './pedidos/pedido.module'

@Module({
  imports: [ParametricasModule, PedidosModule],
})
export class ApplicationModule {}
