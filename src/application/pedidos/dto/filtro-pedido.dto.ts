import { ApiProperty } from '@nestjs/swagger'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

export class PedidoFiltro extends PaginacionQueryDto {
  @ApiProperty({ example: '2023-10-22' })
  fecha: string
}
