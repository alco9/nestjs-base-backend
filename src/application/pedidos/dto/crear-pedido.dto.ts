import { ApiProperty } from '@nestjs/swagger'
import { IsDateString, IsNotEmpty } from 'class-validator'

export class CrearPedidoDto {
  @ApiProperty({ example: 123 })
  @IsNotEmpty()
  nroPedido: number

  @ApiProperty({ example: '2023-10-23' })
  @IsNotEmpty()
  @IsDateString()
  fechaPedido: string
}
