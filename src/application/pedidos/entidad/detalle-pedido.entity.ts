import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { DetallePedidoEstado } from '../constant'

dotenv.config()

@Check(UtilService.buildStatusCheck(DetallePedidoEstado))
@Entity({ name: 'detalles_pedido', schema: process.env.DB_SCHEMA_PEDIDOS })
export class DetallePedido extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla DetallePedido',
  })
  id: string

  @Column({
    name: 'id_producto',
    type: 'bigint',
    comment: 'Clave foránea de la tabla producto',
  })
  idProducto: string

  @Column({
    name: 'id_pedido',
    type: 'bigint',
    comment: 'Clave foránea de la tabla pedido',
  })
  idPedido: string

  constructor(data?: Partial<DetallePedido>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || DetallePedidoEstado.ACTIVO
  }
}
