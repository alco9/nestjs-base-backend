import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { PedidoEstado } from '../constant'

dotenv.config()

@Check(UtilService.buildStatusCheck(PedidoEstado))
@Entity({ name: 'pedidos', schema: process.env.DB_SCHEMA_PRODUCTOS })
export class Pedido extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Pedido',
  })
  id: string

  @Column({
    name: 'nro_pedido',
    type: 'int',
    unique: true,
    comment: 'Nro. de pedido',
  })
  nroPedido: string

  @Column({ length: 50, type: 'varchar', comment: 'Nombre de producto' })
  nombre: string

  @Column({
    name: 'fecha_pedido',
    type: 'timestamp without time zone',
    comment: 'Precio unitario',
  })
  fechaPedido: Date

  constructor(data?: Partial<Pedido>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || PedidoEstado.ACTIVO
  }
}
