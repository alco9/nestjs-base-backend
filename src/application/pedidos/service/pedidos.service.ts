import { CrearPedidoDto, PedidoFiltro } from 'src/application/pedidos/dto'
import { ProductosService } from '../../../application/productos/service'
import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable } from '@nestjs/common'
import { PedidoRepository } from '../repository'

@Injectable()
export class PedidoService extends BaseService {
  constructor(
    private productosService: ProductosService,
    @Inject(PedidoRepository)
    private pedidoRepositorio: PedidoRepository
  ) {
    super()
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    const result = await this.pedidoRepositorio.crear(
      datosPedido,
      usuarioAuditoria
    )
    return result
  }

  async listar(filtro: PedidoFiltro) {
    const result = await this.pedidoRepositorio.listar(filtro)
    return result
  }
}
