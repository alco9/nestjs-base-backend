import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards
} from '@nestjs/common'
import { Request } from 'express'
import { BaseController } from '../../../common/base'
import { ParamIdDto } from '../../../common/dto/params-id.dto'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { ProductosService } from '../service'
import { FiltroProducto } from '../dto/filtro-producto.dto'

@Controller('productos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class ProductosController extends BaseController {
  constructor(private productosServicio: ProductosService) {
    super()
  }

  @Get()
  async listar(@Query() consulta: FiltroProducto) {
    const result = await this.productosServicio.listar(consulta)
    return this.successListRows(result)
  }

  @Get(':id')
  async buscarPorId(@Param() params: ParamIdDto) {
    const { id: idProducto } = params
    const result = await this.productosServicio.buscarPorId(idProducto)
    return this.success(result)
  }

  @Post()
  async crear(@Req() req: Request, @Body() productoDto: CrearProductoDto) {
    // const usuarioAuditoria = this.getUser(req)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.crear(
      productoDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }

  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() productoDto: ActualizarProductoDto
  ) {
    const { id: idProducto } = params
    // const usuarioAuditoria = this.getUser(req)
    const usuarioAuditoria = '1'
    const result = await this.productosServicio.actualizarDatos(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Delete(':id')
  async eliminar(@Param('id') id: string) {
    const result = await this.productosServicio.eliminar(id)
    return this.successDelete(result)
  }
}
