import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ProductoEstado } from '../constant'

dotenv.config()

@Check(UtilService.buildStatusCheck(ProductoEstado))
@Entity({ name: 'productos', schema: process.env.DB_SCHEMA_PRODUCTOS })
export class Producto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Producto',
  })
  id: string

  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Código de producto',
  })
  codigo: string

  @Column({ length: 30, type: 'varchar', comment: 'Categoría de producto' })
  categoria: string

  @Column({ length: 50, type: 'varchar', comment: 'Nombre de producto' })
  nombre: string

  @Column({ type: 'float', comment: 'Precio unitario' })
  precio: number

  constructor(data?: Partial<Producto>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ProductoEstado.ACTIVO
  }
}
