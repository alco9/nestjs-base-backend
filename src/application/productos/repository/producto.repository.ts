import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { Producto } from '../entity'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import { FiltroProducto } from '../dto/filtro-producto.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const { codigo, categoria, nombre, precio } = productoDto

    const producto = new Producto()
    producto.codigo = codigo
    producto.categoria = categoria
    producto.nombre = nombre
    producto.precio = precio
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async listar(filtroProductos: FiltroProducto) {
    const { limite, saltar, categoria, nombre } = filtroProductos
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.categoria',
        'producto.nombre',
        'producto.precio',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)

    if (categoria) {
      query.andWhere('producto.categoria = :categoria', { categoria })
    }
    if (nombre) {
      query.andWhere('producto.nombre LIKE :nombre', {
        nombre: `%${nombre}%`,
      })
    }

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const { codigo, categoria, nombre, precio } = productoDto
    const datosActualizar: QueryDeepPartialEntity<Producto> = new Producto({
      codigo: codigo,
      categoria: categoria,
      nombre: nombre,
      precio: precio,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }

  async eliminar(id: string) {
    return await this.dataSource.getRepository(Producto).delete(id)
  }
}
