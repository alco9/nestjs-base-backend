import { BaseService } from '../../../common/base/base-service'
import {
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { ProductoRepository } from '../repository'
import { CrearProductoDto } from '../dto'
import { ActualizarProductoDto } from '../dto'
import { FiltroProducto } from '../dto/filtro-producto.dto'

@Injectable()
export class ProductosService extends BaseService {
  constructor(
    @Inject(ProductoRepository)
    private productoRepositorio: ProductoRepository
  ) {
    super()
  }

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const productoRepetido = await this.productoRepositorio.buscarCodigo(
      productoDto.codigo
    )

    if (productoRepetido) {
      throw new ConflictException(
        `Ya existe un producto con el codigo "${productoDto.codigo}"`
      )
    }

    return await this.productoRepositorio.crear(productoDto, usuarioAuditoria)
  }

  async listar(filtro: FiltroProducto) {
    return await this.productoRepositorio.listar(filtro)
  }

  async actualizarDatos(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    await this.productoRepositorio.actualizar(id, productoDto, usuarioAuditoria)
    return { id }
  }

  async buscarPorId(id: string) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    return producto
  }

  async eliminar(id: string) {
    return await this.productoRepositorio.eliminar(id)
  }
}
