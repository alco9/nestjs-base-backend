import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from '../../../common/validation'

export class ActualizarProductoDto {
  @ApiProperty({ example: 'A-001' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: 'Categoría del producto' })
  @IsNotEmpty()
  categoria: string

  @ApiProperty({ example: 'Nombre del producto' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: '12.99' })
  @IsNotEmpty()
  precio: number
}
