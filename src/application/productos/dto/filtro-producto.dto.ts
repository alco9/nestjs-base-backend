import { ApiProperty } from '@nestjs/swagger'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

export class FiltroProducto extends PaginacionQueryDto {
  @ApiProperty({ example: 'Accesorios' })
  categoria: string

  @ApiProperty({ example: 'Audífonos inalámbricos' })
  nombre: string
}
